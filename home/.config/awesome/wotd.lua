local wibox = require("wibox")
local wotd = wibox.widget {
    widget = wibox.widget.textbox,
    markup = "<b><big>daedalus</big></b>",
    font = "Deja Vu Sans Mono 10"
}
return wotd
