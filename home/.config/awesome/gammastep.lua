-- standard libraries
local awful = require("awful")

-- variables
local gammastep = {}
gammastep.bin = "/usr/bin/gammastep"
gammastep.pkill = "/usr/bin/pkill"
gammastep.state = 1

-- functions
gammastep.start = function()
    awful.spawn(gammastep.bin)
    gammastep.state = 1
end

gammastep.stop = function()
    awful.spawn(gammastep.bin .. " -x")
    awful.spawn(gammastep.pkill .. " gammastep")
    gammastep.state = 0
end

gammastep.toggle = function()
    if gammastep.state == 1
    then
        gammastep.stop()
    else
        gammastep.start()
    end
end

gammastep.init = function(initState)
    if initState == 1
    then
        gammastep.start()
    else
        gammastep.stop()
    end
end

return gammastep
