# rice

Desktop ricing repository

## alacritty

Configuration contains font and solarized color scheme settings.

## awesome

Configuration contains solarized theme and rc file.

It was based on default theme and rc file, modified to my preferences
with a solarized colour
scheme. [Link](https://en.wikipedia.org/wiki/Solarized).

The background dark shade differs slightly from the normal solarized
to match the shade of a wallpaper.

This setup requires:
- gammastep
- [battery-widget](https://github.com/deficient/battery-widget)
- `solarized_debian.png` inside awesome's user configuration directory

Other recommended programs:
- firefox
- cherrytree
- i3lock-fancy
- flameshot

## rofi

Configuration contains solarized and tweaked rofi launcher.
